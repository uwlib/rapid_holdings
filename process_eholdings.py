#!/usr/bin/env python
"""
Script for processing data/files related to reporting of electronic
holdings data for RAPID upload. See external documentation for overall
workflow and input requirements.
"""
# -*- coding: utf-8 -*-

from __future__ import print_function
from lxml import etree

import sys
import time
import textwrap
import os.path
import csv
import codecs
import cStringIO

class UTF8Recoder:
    """
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    """
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")

class CSVUnicodeReader:
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self

class CSVUnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

# used by wrap_print to format long lines of text
wrapper = textwrap.TextWrapper(width=65, subsequent_indent='   ', \
                               break_on_hyphens=False)
def wrap_print(str):
    """
    Helper function to make sure that long print statement wrap nicely.
    Any text passed into this will be at most 65 characters in width with
    a hanging indent of 3 spaces.
    """
    print(wrapper.fill(str))

def usage(where=sys.stdout):
    """Print a usage statement for this script."""
    print('Usage:', file=where)
    print('  process_eholdings.py <prev_inte...es.csv> <modif...es.csv> <exp...ngs.xml>')
    print('Where:', file=where)
    print('  prev_inte...es.csv   Previous interface mapping file sent to RAPID', file=where)
    print('  modif...es.csv       Analytic data on licenses changed since last update', file=where)
    print('  exp...ngs.xml        Google Scholar XML export of e-holdings', file=where)
    print('Output:', file=where)
    print('  interfacesOnly-exp...ngs.xml      XML file with interfaced items only', file=where)
    print('  cleaned-<...>.csv                 Reformated license file', file=where)
    print('  removeInterfaces.<date>.csv       Any interfaces no longer in Alma', file=where)
    print('  allInterfacesForRapid.<date>.csv  RAPID submision with license checking info', file=where)

def fileCheck(filename):
    if not os.path.isfile(filename):
        print('File: ' + filename + ' not found. Exiting...', file=sys.stderr)
        sys.exit(1)

def main(argv):
    if len(argv) != 4:
        usage(sys.stderr)
        sys.exit(1)      
    
    # input files
    previousInterfacesFile = argv[1]     # 'allInterfacesForRapid20140318.csv'
    modifiedLicensesFile = argv[2]       # 'License_Modification_Dates.csv'
    electronicHoldingsXMLFile = argv[3]  #'electronic-holdings.2015.04.09.xml'

    # check file paths
    fileCheck(previousInterfacesFile)
    fileCheck(modifiedLicensesFile)
    fileCheck(electronicHoldingsXMLFile)
    
    # output files
    interfaceItemsOnlyXMLFile = 'interfacesOnly-' + electronicHoldingsXMLFile
    cleanedModifiedLicensesFile = 'cleaned-' + modifiedLicensesFile
    removedInterfacesFile = 'removedInterfaces.' + time.strftime("%Y.%m.%d") + '.csv'
    newInterfacesMappingFile = 'allInterfacesForRapid.' + time.strftime("%Y.%m.%d") + '.csv'
    
    # sets/maps to fill in
    prevInterfaceMap = {}         # interface_name -> Yes/No
    currInterfacesSet = set()     # interfaces found in electronicHoldingsXMLFile
    prevInterfacesSet = set()     # interfaces found in previousInterfacesFile
    updatedLicenses = set()       # interfaces with updated licenses in modifiedLicensesFile


    #--------------------------------------------------------------------------------#
    # strip interfaces from the exported holdings for current interfaces
    #--------------------------------------------------------------------------------#

    wrap_print('Loading current electronic holdings from: ' + electronicHoldingsXMLFile)

    # load the holdings XML file
    xml = etree.parse(electronicHoldingsXMLFile)

    wrap_print('Grabbing current interfaces from XML')

    # use Xpath to find all <interface_name> tags and store their text in currInterfacesSet
    for e in xml.findall('.//' + '{' + xml.getroot().nsmap[None] + '}' + 'interface_name'):
        currInterfacesSet.add(e.text.strip())


    #--------------------------------------------------------------------------------#
    # remove items from xml without interface_name element
    #--------------------------------------------------------------------------------#
    
    wrap_print('Cleaning electronic holdings XML...')

    # iterate over each item and remove it if it does not contain an <interface_name> child
    for e in xml.findall('.//' + '{' + xml.getroot().nsmap[None] + '}' + 'item'):
        if(e.find('.//' + '{' + xml.getroot().nsmap[None] + '}' + 'interface_name') is None):
           e.getparent().remove(e)
           
    wrap_print('Writing cleaned electronic holdings XML to: ' + interfaceItemsOnlyXMLFile)

    # write the condensed XML holdings to file
    xml_writer = codecs.open(interfaceItemsOnlyXMLFile, 'w', encoding='utf-8')
    xml_writer.write(etree.tostring(xml.getroot(), pretty_print=True))


    #--------------------------------------------------------------------------------#
    # load modified licenses, create a set for later, and write out a nice version
    #--------------------------------------------------------------------------------#
    
    wrap_print('Loading modified license information from: ' + modifiedLicensesFile);

    # read in and output a clean version of the modified license information
    csvReader = CSVUnicodeReader(open(modifiedLicensesFile, 'rb'))
    csvWriter = CSVUnicodeWriter(open(cleanedModifiedLicensesFile, 'wb'))

    # write headers for the cleaned CSV file
    csvWriter.writerow(['Interface', 'Type', 'Code', 'Date'])
    
    firstRow = True # used to skip the first row (headers)
    for row in csvReader:
        if firstRow:
            firstRow = False
            continue
        interface = row[0].strip()

        # add the interface to the updatedLicenses set
        updatedLicenses.add(interface)

        # write out a row in the cleaned file for each electronic collection or portfolio update
        cCode = row[1].strip()
        cDate = row[2].strip()
        pCode = row[3].strip()
        pDate = row[4].strip()               
        if cCode != '':
            csvWriter.writerow([interface, 'EC', cCode, cDate])
        if pCode != '':
            csvWriter.writerow([interface, 'EP', pCode, pDate])
            
    wrap_print('Writing cleaned modified license information to: ' + cleanedModifiedLicensesFile)


    #--------------------------------------------------------------------------------#
    # load previous interface mapping
    #--------------------------------------------------------------------------------#    

    wrap_print('Loading previous interface-ILL mapping from: ' + previousInterfacesFile)

    # load the previous interface file
    # map each pair (interface name, Yes/No)
    # also store the interface names in prevInterfacesSet for easier calcs later
    csvReader = CSVUnicodeReader(open(previousInterfacesFile, 'rb'))    
    firstRow = True
    for row in csvReader:
        if firstRow:
            firstRow = False
            continue        
        prevInterfaceMap[ row[0].strip() ] = row[1].strip()
        prevInterfacesSet.add( row[0].strip() )

    print()

    #--------------------------------------------------------------------------------#            
    # compare the previous and current interfaces
    #--------------------------------------------------------------------------------#    

    wrap_print('Processing...')

    # perform set differences and intersection to determine which interfaces have been
    # added, removed, or kept the same
    removedInterfaces = prevInterfacesSet - currInterfacesSet
    addedInterfaces = currInterfacesSet - prevInterfacesSet
    sameInterfaces = prevInterfacesSet & currInterfacesSet

    print()

    #--------------------------------------------------------------------------------#
    # write the removed interfaces
    #--------------------------------------------------------------------------------#    

    wrap_print('Writing all removed interfaces information to: ' + removedInterfacesFile)

    csvWriter = CSVUnicodeWriter(open(removedInterfacesFile, 'wb'))
    csvWriter.writerow(['Removed Interface', 'Previous ILL Status'])   # header row
    for i in sorted(removedInterfaces): 
        # store the previous lendability information for convenience
        csvWriter.writerow([ i , prevInterfaceMap.get(i) ])

    #--------------------------------------------------------------------------------#    
    # generate a new Interfaces File
    #--------------------------------------------------------------------------------#
    
    wrap_print('Writing new interface-ILL mapping file to: ' + newInterfacesMappingFile)    
    
    csvWriter = CSVUnicodeWriter(open(newInterfacesMappingFile, 'wb'))

    # Output is like standard lendability mapping except new and updated license
    # interfaces are tagged with a message in a third column
    csvWriter.writerow(['Interface', 'ILL Status', 'Status'])   # header row
    for i in sorted(sameInterfaces | addedInterfaces): 
        mapping = prevInterfaceMap.get(i, '')
        if i in addedInterfaces:
            status = 'NEW'
        elif i in updatedLicenses:
            status = 'CHECK'
        else:
            status = '' # don't forget that python does not have local scoping with loops
        csvWriter.writerow([i, mapping, status])


    print('Done')

    
if __name__ == '__main__':
    main(sys.argv)
