
# Workflow for Generating Print Holdings

The general instructions for RAPID handling can be found on the [RAPID Update Info Hole page](https://staffweb.lib.washington.edu/units/ITS/info-hole/procedures/alma-procedures/rapid-holdings-update).

## What RAPID Needs 
For our print serial holdings, we need to provide RAPID with two files:

* A  binary MARC21 file of our print serial holdings
* A CSV file containing a mapping of lendability to library codes and location codes

## What is Needed to Generate the Above
Of course, we can't simply push a button in Alma and generate those files. Instead, we have to provide UW ILL (i.e., Heidi) with a bit of support. Most importantly, the only way currently to generate the lendability mapping is for someone to MANUALLY update the lendability for each location (although this is unlikely to change over time).

The overall workflow involves taking the following two files:

* The most recent location-lendability mapping file sent to RAPID (you can request it from them if no local copy is available)
* An exported binary MARC 21 file from Alma

These files are then processed by the Python script *process_pholdings.py* to generate the following file:

* An updated location-lendability mapping file indicating what locations serials are currently at (and should be checked) as well as any new locations that require information

Note that the this file should hopefully reduce the work that Heidi will have to do.

## Basic Workflow
Fortunately, generating the files for the print holdings is far easier than it is for the electronic holdings. 

### 1. Export the Print Holdings from Alma
1. In Alma, under *Alma\Resource Management\Search and Sets\Manage Sets*, find the public, logical set **RAPID - Print Holdings - Serials**. If it is not there, recreating it is a snap:
	* Create a new *Logical* set with the *content type* set to *Physical Titles*.
	*  Using the *Advanced Search* option, add the condition:  
All titles - Find - Bibliographic level - Equals - Serial     
	* Click *Go* and then *Save*.
	
2. Export the results. 
	* Under *Alma\Administration\Manage Jobs\Run a Job*,  select the **Export Bibliographic Records** job and click **Next**.
	* Find and select the set from the previous set and click **Next**.
	* The export parameters should be set at follows:  
		* Output Format: MARC21 Binary
		* Bibliographic record formats...: MARC21 Bibliographic
		* Number of records in file: One File
		* Expand Routine: Add Holdings Information
		* Export into folder: Private
		* FTP configuration: leave blank
		* Sub-directory: leave blank
	* Click **Next**		
	* Run as soon as possible. Click **Next**.
	* Submit the job.

6. Once the job is complete, download the file to where you will be running the processing script. Note that this download will take a while (upwards of 20 minutes), so enjoy a snack.
	* Go to *Alma\Resource Management\Search and Sets\Manage Exports*
 	* Find the exported set
	* Click *View Files* 
	* Under the *Actions* button, click on **Download** 

### 2. Process the Files 
1. Assemble the following files in the same Linux directory as the **process_eholdings.py** script:
	* The previous location-lending mapping file that was sent to RAPID. This should be available in `\\files\shareddocs\LibrarySystems\CurrentProjects\Rapid\`.
	* The MARC21 binary print holdings file exported from Alma.

2. This script requires the 'pymarc' Python module. To test if it's installed, run the `pip install --user pymarc`. This will tell you if it's installed already. You may also need to install the 'six' module as well.

3. Run the script as follows. Note that MARC processing is slow, so this script will take several minutes to complete.
	
		process_pholdings.py <holdings.mrc> <locations.csv>



3. One CSV file will be generated:  
An updated locations file: *LocationsForRAPID-<date>.csv*

### 4. Send Generated Location CSV file to ILL
Someone in ILL will need to go through the *LocationsForRAPID-<date>.csv* file and update the lendability information for any indicated location.  The structure of that file indicates which interfaces need to be checked:

| Library Code | Location Code | Location Description | Library Name | Holding Format | Prev. Lendability | Curr. Lendability |
| --- | --- | --- | --- | --- | --- | --- |
| ... | ... | ... | ... | ... | Yes | Check |
| ... | ... | ... | ... | ... | No  | Check |
| ... | ... |     |     |     |     | New   |
| ... | ... | ... | ... | ... | Yes | Yes   |
| ... | ... | ... | ... | ... | No  | No    |

The first two columns are from the MARC fields 852b and 852c, respectively.  The next three columns are filled in manually if you wish by looking at location information within Alma. The most important columns are the last two: the previous and current lendability status. If a locations appears in our print holdings, it is tagged as CHECK in the last column. Most likely, ILL will just need to copy over the value from the previous column. If the location is NEW, columns 3-5 are blank, and there is no previous lendability. Finally, if the location does not appear in the print holdings, we keep it around for legacy and and just copy the previous lendability into the current lendability cell.

### 5. Upload Files to RAPID
Once ILL is done updating the lendability status file, the print holdings update can be submitted to RAPID. The following files are what need to be submitted:

* The MARC21 binary file exported from Alma
* The updated location-lendability mapping file (remove the previous lendability column before sending).

Instructions on how to do the upload (ITS generally does it for ILL), see the [Info Hole page](https://staffweb.lib.washington.edu/units/ITS/info-hole/procedures/alma-procedures/rapid-holdings-update).

Be sure to store the location-lendability mapping file for use in the next RAPID update.
