#!/usr/bin/env python
"""
Test
"""
# -*- coding: utf-8 -*-

from __future__ import print_function

from pymarc import MARCReader

import sys
import time
import os.path
import csv
import codecs
import cStringIO

class UTF8Recoder:
    """
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    """
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")

class CSVUnicodeReader:
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self

class CSVUnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

def usage(where=sys.stdout):
    """Print a usage statement for this script."""
    print('Grab locations from a binary MARC21 file and store to a CSV file',
          file=where)    
    print('Usage:', file=where)
    print('  process_pholdings.py <file.mrc> <prev-locations.csv>')
    print('Where:', file=where)
    print('  file.mrc            Binary MARC21 export of print holdings',
          file=where)
    print('  prev-locations.csv  Location-lendability file from previous year',
          file=where)          
    print('Output:', file=where)
    print('  Generates a timestamped CSV file: LocationsForRAPID-<date>.csv', file=where)

def fileCheck(filename):
    if not os.path.isfile(filename):
        print('File: ' + filename + ' not found. Exiting...', file=sys.stderr)
        sys.exit(1)

def main(argv):
    if len(argv) != 3:
        usage(sys.stderr)
        sys.exit(1)

    # input files
    inMarcFile = argv[1]
    prevLocationsFile = argv[2]

    # filecheck inputs
    fileCheck(inMarcFile)
    fileCheck(prevLocationsFile)
    
    # output file
    newLocationsFile = 'LocationsForRAPID-' + time.strftime("%Y.%m.%d") + '.csv'

    # sets/maps to fill in
    currLocationsSet = set()    
    prevLocationsSet = set()
    prevLocationsMap = {}    

    #--------------------------------------------------------------------------------#    
    # Parse the MARC file for the locations
    #--------------------------------------------------------------------------------#

    marcReader = MARCReader(file(inMarcFile), to_unicode=True, force_utf8=True)

    print('Processing MARC entries for location information...')
    
    i = 0 # keep track of progress and report as we go
    for record in marcReader:
        i = i + 1
        if record['852'] is not None:
            locB = "NONE" if record['852']['b'] is None else record['852']['b']
            locC = "NONE" if record['852']['c'] is None else record['852']['c']
            currLocationsSet.add( (locB, locC) )
        if i % 2500 == 0:
            print('  ' + str(i) + ' records processed. ' + \
                  str(len(currLocationsSet)) + ' locations found.')
    
    
    #--------------------------------------------------------------------------------#
    # Loading previous locations file
    #--------------------------------------------------------------------------------#
    
    print('Loading previous location files...')

    csvReader = CSVUnicodeReader(open(prevLocationsFile, 'rb'))
    
    # get the headers to reuse
    headers = csvReader.next()    
    # headers - 2 for the library/location codes
    prevDataLength = len(headers) - 2;
    
    for row in csvReader:
        loc = (row[0], row[1])
        prevLocationsSet.add(loc)
        prevLocationsMap[ loc ] = row[2:]


    #--------------------------------------------------------------------------------#
    # Merge current locations information
    #--------------------------------------------------------------------------------#

    print('Generating new location lendability file...')
    
    headers.append('Lendable - ' + time.strftime("%Y.%m.%d"))
    csvWriter = CSVUnicodeWriter(open(newLocationsFile, 'wb'))
    csvWriter.writerow(headers)
    
    for loc in sorted( prevLocationsSet | currLocationsSet):
        row = [ loc[0], loc[1] ]
        # copy the descriptive columns and lendability if we already know the location
        if loc in prevLocationsSet:
            row.extend( prevLocationsMap.get(loc) )
        else:
            row.extend( [''] * prevDataLength )

        # label any current location as Check or New depending on previous information
        if loc in currLocationsSet:
            if row[-1] == '': # 
                row.append('NEW')
            else:
                row.append('CHECK')
        else:
            row.append( row[-1] )

        csvWriter.writerow(row)
    
    print('Finished.')

if __name__ == '__main__':
    main(sys.argv)
