
# Workflow for Generating Electronic Holdings

The general instructions for RAPID handling can be found on the [RAPID Update Info Hole page](https://staffweb.lib.washington.edu/units/ITS/info-hole/procedures/alma-procedures/rapid-holdings-update).

## What RAPID Needs 
For our electronic serial holdings, we need to provide RAPID with two files:

* A Google Scholar XML file containing the interface names
* A CSV file containing a mapping of lendability for each of the interface names (interface_name, Y/N)

## What is Needed to Generate the Above
Of course, we can't simply push a button in Alma and generate those two files. Instead, we have to provide UW ILL (i.e., Heidi) with a bit of support. Most importantly, the only way currently to generate the lendability mapping is for someone to MANUALLY check the licensing information for each interface. The goal of this workflow is to try to reduce that headache.

The overall workflow involves taking the following three files:

* The most recent lendability mapping file sent to RAPID (you can request it from them if no local copy is available)
* An exported Google Scholar XML file from Alma
* Exported CSV file from Alma Analytics containing a list interface licenses that were modified after the previous RAPID update

These files are then processed by the Python script *process_eholdings.py* to generate the following files:

* An XML file containing only items that are interfaced
* A cleaned/better-formatted CSV file containing the recently modified licenses
* A CSV file containing a list of interfaces that are no longer in our holdings
* A CSV containing the interface mapping data from last year (sans removed holdings) with indicators for any newly added interfaces and interfaces whose licenses have been modified

Note that the last file should hopefully reduce the work that Heidi will have to do.

## Basic Workflow
The following steps assume that the indicated Alma set and analytic are readily available. If not, please refer to Appendix at the end of this file.

### 1. Export the Electronic Holdings from Alma
1. In Alma, under *Alma\Resource Management\Search and Sets\Manage Sets*, find the public, logical set **RAPID - Electronic Holdings - Available Serials** and choose the *Results* option under *Actions*.

2. Most likely, you will get an internal error message. This is expected and just requires a reset. If the progress bar fails to go away, close it by clicking the X. Next, click on *Change Query*. On the next screen, the search needs to be amended as follows:    
    * Make sure that you have the Repository Manager role (otherwise you'll have to create the set from scratch as described in the appendix).
	* Change the *Find* dropdown from **All titles** to **Electronic titles**.
    * Ensure that three search conditions are present. Add/edit if they are not:
           - All titles - Find - Bibliographic level - Equals - Serial           
           - Electronic Portfolio - Find - Availability - Equals - Available
           - Electronic Collection - Find - Availability - Equals - Available

3. Click **Go** and then **Save Query**.

4. Because this logical set is fragile, it is recommended that you Itemize it right away. This also means you get a snapshot of the e-holdings for the day you updated RAPID. To Itemize, choose the *Itemize* option under *Action*    and follow the indicated steps.

5. Finally, export the results. 
	* Under *Alma\Administration\Manage Jobs\Run a Job*,  select the **Export Electronic Portfolio** job and click **NEXT**.
	* Find and select the itemized set you created in *Step 4* and click **Next**.
	* Export the data as *Google Scholar XML schema* as *One File* into the *Private* folder. Be sure to **CHECK** the *Add interface name* option. The other parameter options can be left blank. Click **Next**.
	* Run as soon as possible. Click **Next**.
	* Submit the job.

6. Once the job is complete, download the file to where you will be running the processing script. To download:
	* Go to *Alma\Resource Management\Search and Sets\Manage Exports*
 	* Find the exported set
	* Click *View Files* 
	* Under the *Actions* button, click on **Download**
 

### 2. Get Updated License Information from Alma Analytics
1. In *Alma Analytics*, find and open the prepared analytic **License_Modification_Dates**. It should be saved under the path */shared/University of Washington/Reports/ITS/deibel_RAPID/*

2. In the *edit view*, go to the *Criteria* tab. Make sure that the two **Modification Date** filters are set to the proper day. The day should be the date of the previous RAPID update. The date of the previous RAPID update can be found by looking at datestamps and filenames in `\\files\shareddocs\LibrarySystems\CurrentProjects\Rapid\`. If you need to edit the date, hover over a filter and click on the pencil icon to open the *Edit Filter* menu. Enter the date manually (mm/dd/yyyy format) in the *Value* input or click on the calendar icon to the right of the input.

3.  Go to the *Results* tab and export the data as a CSV file. Under the *Export* menu (icon is paper with an upwards pointing arrow), select *Data* then *CSV Format*.

4. Save the CSV file to where you be running the processing script.
    
### 3. Process the Files 
1. Assemble the following files in the same Linux directory as the **process_eholdings.py** script:
	* The previous lending interface mapping file that was sent to RAPID. This should be available in `\\files\shareddocs\LibrarySystems\CurrentProjects\Rapid\`.
	* The XML e-holdings file exported from Alma.
	* The license update CSV file exported from Alma Analytics.

2. Run the script as follows:
	
		process_eholdings.py <prevMapping.csv> <licenses.csv> <holdings.xml>

3. Three CSV files and one XML file will be generated:
	* A reduced XML file containing only items that have an interface: *interfacesOnly- ... .xml*
	* A reformatted license file: *cleaned- ... .csv*
	* A list of interfaces no longer present in the e-holdings:  *removeInterfaces.<date>.csv*
	* A list of interfaces in our current e-holdings to be checked by ILL:  *allInterfacesForRapid.<date>.csv*

### 4. Send Generated CSV files to ILL
Someone in ILL will need to go through the *allInterfacesForRapid.<date>.csv* file and update the lendability information for any indicated interface.  The structure of that file indicates which interfaces need to be checked:

| Interface Name  | ILL Status | Status |
| --------------- | ---------- | ------ |
| Interface\_1     | Yes        |        |
| Interface\_2     | No         | CHANGED |
| Interface\_3     | No         |  |
| Interface\_4     |            | NEW |

The first column is the name of the interface and the second column is the ILL lending status. If the licensing information has not changed (as with Interface\_1 and Interface\_3), the third column is left blank. If the status has CHANGED (Interface\_2), the lendability rules need to be checked manually. Note that the previous ILL status is shown for convenience. Finally, if the status is a NEW interface (Interface\_4), the second column is blank as there is no previous data on the interface.

It is worthwhile to also send to ILL the other CSV files, *cleaned- ... .csv* and *removeInterfaces.<date>.csv*, as these may be helpful when updating the licenses. For example, a license may appear removed because of a spelling change. 

### 5. Upload Files to RAPID
Once ILL is done updating the lendability status file, the e-holdings update can be submitted to RAPID. The following files are what need to be submitted:

* The interfaces-only Google Scholar XML file generated by the processing script
* The updated interface-lendability mapping file (remove the third column STATUS before sending).

Instructions on how to do the upload (ITS generally does it for ILL), see the [Info Hole page](https://staffweb.lib.washington.edu/units/ITS/info-hole/procedures/alma-procedures/rapid-holdings-update).

Be sure to also store the interface-lendability mapping file for use in the next RAPID update.


    
## Appendix: Recreating the Alma Set and Analytic

Hopefully, the set and analytic used for doing the above will always be available. Of course, reality is that one or both may have to be recreated. Here's how to do so.

### Creating the E-Holdings Set
The fundamental challenge here is that we store interface information in both our *Electronic Portfolios* and our *Electronic Collections*. Normally, this would be easily solved by just creating two sets and the combining them. Unfortunately, the export job we need to use is *Export Electronic Portfolio* job that only works with sets generated originally as *All Titles* or *Electronic Portfolios*. Thus, we have to work around the issue.

1.  Create a new *Logical* set with the *content type* set to *All Titles*.
2.  Using the *Advanced Search* option, add the condition:  
All titles - Find - Bibliographic level - Equals - Serial     
3. Click *Go* and then *Save*.
4. Now is when we'll add in the focus on electronic items. Select *Results* in the *Action* menu for the set you just created. 
5. Click on *Change Query* and then do the following tasks:
    * Change the *Find* dropdown from **All titles** to **Electronic titles**.
    * Ensure that three search conditions are present. Add/edit if they are not:
           - All titles - Find - Bibliographic level - Equals - Serial           
           - Electronic Portfolio - Find - Availability - Equals - Available
           - Electronic Collection - Find - Availability - Equals - Available
6. Click *Go* and then *Save Query*

As noted above, you'll probably have to redo steps 5 and 6 each time you need to use the set. Because we change the set's content type from *All titles* to *Electronic titles*, this apparently makes the logical query unstable. This is why itemizing before exporting is recommended. 

And just in case you think we could get away with setting the content type to **Electronic Titles* from the start, we cannot because that export job does not run on such sets. **NOTE:** This might no longer be true as Alma has made the export process less touchy. 

### Recreating the Modified Licenses Analytic
Fortunately, creating the analytic is much easier and does not require bending any rules. 

1. In Alma Analytics, create a new *Analysis* for *E-Inventory*. 
2. The following fields need to be added (in the indicated order) to the *Selected Columns* pane in the *Criteria* tab
	* Vendor Interface --> Interface Name
	* Electronic Collection License --> Code
	* Electronic Collection License --> Modification Date
	* Portfolio License --> Code
	* Portfolio License --> Modification Date
3. The following fields then need to be added to the *Filters* pane. Note that the **AND** and **OR** below are necessary for correct functioning.
	* Electronic Collection Type --> Description *equals* Aggregator package; Selective package
	* **AND** Electronic Collection License --> Modification Date *is greater than or equal to* <PREVIOUS RAPID DATE>
	* * **OR** Portfolio License --> Modification Date *is greater than or equal to* <PREVIOUS RAPID DATE>

At this point, your analytic should like the screenshot below:

![Screenshot of Modified Licenses analytic configuration](https://bitbucket.org/uwlib/rapid_holdings/raw/master/modified-loans-analytic.png "Modified licenses analytic configuration")

Save the analytic and you're golden.

